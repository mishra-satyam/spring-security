package security.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import java.util.WeakHashMap;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {

	@Autowired
	private CustomOAuth2UserService customOAuth2UserService;

	@Autowired
	private OAuth2LoginSuccessHandler oAuth2LoginSuccessHandler;


	@Autowired
	private OAuth2LogoutSuccessHandler oAuth2LogoutSuccessHandler;


//	@Override
//	public void configure (HttpSecurity httpSecurity) throws Exception {
//		httpSecurity.antMatcher("/**").authorizeRequests()
//				.antMatchers("/").permitAll()
//				.anyRequest().authenticated()
//				.and()
//				.oauth2Login().userInfoEndpoint().userService(customOAuth2UserService)
//				.and()
//				.successHandler(oAuth2LoginSuccessHandler)
//				.and().logout().logoutSuccessHandler(oAuth2LogoutSuccessHandler);
//	}

	@Bean
	public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
//		httpSecurity.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
		httpSecurity.csrf().disable()
				.antMatcher("/**").authorizeRequests()
				.antMatchers("/").permitAll()
				.anyRequest().authenticated()
				.and()
				.oauth2Login().userInfoEndpoint().userService(customOAuth2UserService)
				.and()
				.successHandler(oAuth2LoginSuccessHandler)
				.and().logout().logoutSuccessHandler(oAuth2LogoutSuccessHandler);
		return httpSecurity.build();
	}

//	@Bean
//	public TokenAuthenticationFilter tokenAuthenticationFilter() {
//		return new TokenAuthenticationFilter();
//	}



//	@Bean
//	public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
//		httpSecurity
//				.csrf().disable()
//				.authorizeRequests()
//				.anyRequest().authenticated()
//				.and().formLogin();
//		return httpSecurity.build();
//	}

//	@Override
//	public void configure (HttpSecurity httpSecurity) throws Exception {
//		httpSecurity.authorizeRequests()
//				.anyRequest().authenticated()
//				.and().formLogin();
//	}

//	@Bean
//	public InMemoryUserDetailsManager userDetailsService() {
//		UserDetails user = User.withDefaultPasswordEncoder()
//				.username("user")
//				.password("password")
//				.roles("USER")
//				.build();
//		return new InMemoryUserDetailsManager(user);
//	}
}

