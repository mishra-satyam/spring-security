package security.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(maxAge = 3600)
public class Controller {

	@GetMapping("/")
	public String home() {
		return "Welcome home";
	}

	@GetMapping("/user")
	public String user() {
		return "Welcome user";
	}

	@GetMapping("/admin")
	public String admin() {
		return "Welcome admin";
	}
}
